const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const TerserPlugin = require("terser-webpack-plugin");

module.exports = {
    entry:"./src/index.js",
    output:{
        filename:"main.js",
        path: path.resolve(__dirname,"dist")
    },
    plugins:[new HtmlWebpackPlugin(
        {
            template: './src/index.html'
          }
    )],
    mode: 'none',
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        port: 9000,
        hot: true,
    },
    module: {
      rules: [
        {
          test: /\.css$/i,
          use: ["style-loader", "css-loader"],
        },
        {
            test: /\.(jpg|png)$/i,
            use: {loader:"url-loader"}
        }
      ],
    },
  };