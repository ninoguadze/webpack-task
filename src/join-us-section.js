import {default as validate} from './email-validator.js';

class SectionFactory {
    constructor(headerName, buttonText) {
        const section = document.createElement('section');
        section.className = 'app-section app-section--image-join';

        const h2 = document.createElement('h2');
        h2.className = 'app-title';
        h2.textContent = headerName;

        const h3 = document.createElement('h3');
        h3.className = 'app-subtitle';
        h3.innerHTML = 'Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.';

        const form = document.createElement('form');
        form.className = "join-form"
        
        form.addEventListener('submit', function (event) {
            event.preventDefault();
            const emailInput = event.target.elements['email'];
            console.log('Entered value:', validate(emailInput.value));
        });

        const input = document.createElement('input');
        input.type = 'email';
        input.name = 'email';
        input.id = 'email-getter'
        input.className = 'app-section__input app-section__input--email';
        input.placeholder = 'Email';

        const button = document.createElement('button');
        button.className = 'app-section__button app-section__button--subscribe';
        button.id = 'subscribe-button'
        button.textContent = buttonText;

        form.appendChild(input);
        form.appendChild(button);

        section.appendChild(h2);
        section.appendChild(h3);
        section.appendChild(form);

        this.section = section;
    }
    remove() {
        this.section.parentNode.removeChild(this.section);
    };
}

class StandardSectionFactory extends SectionFactory {
    constructor() {
        super("Join Our Programm", "Subscribe");
    }
}

class AdvancedSectionFactory extends SectionFactory {
    constructor() {
        super("Join Our Advanced Programm", "Subscribe to the advanced")
    }
}

const section = (type) => {
    switch (type) {
        case 'standard':
            let x = new StandardSectionFactory();
            return x.section;
        case 'advanced':
            let y = new AdvancedSectionFactory();
            return y.section;
        default:
            throw new Error('Invalid section type.');
    }
}

export { section }